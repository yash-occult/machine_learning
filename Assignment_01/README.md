# README #


### Task ###
Train your favorite ml model to calculate y=5x+3.


### Steps ###
* Created a random inputs based on the equation
* On adding value of X we get y
* Created a dataframe using X and y
* Created training data and testing data
* Tested 3 Machine learinng Models( Linear Regression,Ridge Regression, DecisionTreeRegressor, SVR)
* Added Noise and outlier to data
* Plotted graph for test data and prediction

### Observation ###
#### Noisy Data ####
* For Linear regression, We can say that noise dint affect much, But our coefficent and intercept changed.
* For Decision tree, we can say that noisy data dint affect the model performance.
* Ridge regression works well for corelation of data in large dataset, Casue we have lesser features it works same like linear regression. So it gives same result like Linear Regression
* SVR has no effect of noisy data
* Knn is sensetive to noisy data.


#### Outlier Data ####
* Linear regression can not deal with outlier hence the score changed drasticaly on adding outlier
* For decission tree, We can say that outlier affected it, With a test score of 75%
* Ridge regression works well for corelation of data in large dataset, Casue we have lesser features it works same like linear regression, Outlier
* SVR has no effect of outlier data
* Outlier affects knn performance and due to paramter tuning it takes lot time to train on this model.

#### Best Performance ####
* SVR Performed the best out of all the model, But it can not be used for large dataset.